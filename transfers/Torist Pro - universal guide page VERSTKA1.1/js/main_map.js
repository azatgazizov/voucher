var searchMarkers = {};
var search_map;

ymaps.ready(init);

function init() {

  search_map = new ymaps.Map("map_search", {
    center: [55.76, 37.64],
    zoom: 10,
    controls: ['zoomControl'],
  });

  search_map.events.add('click', function () {
    search_map.balloon.close();
  });

  search_map.behaviors.disable('scrollZoom');

  var templateDefault = ymaps.templateLayoutFactory.createClass(
    '<div class="search_mark_hotel"><img src="images/beds.svg"/>$[properties.iconContent]</div>'
  );


  var myBalloonContentBodyLayout = ymaps.templateLayoutFactory.createClass(
    '<a class="search_inner" href="$[properties.link]">' +
    '<img src="$[properties.image]"><div class="desc">' +
    '<div class="stars" data-fill="$[properties.stars]"><div class="star-item"></div><div class="star-item"></div><div class="star-item"></div><div class="star-item"></div><div class="star-item"></div></div>' +
    '<div class="search_name">$[properties.name]</div>' +
    '<div class="search_reviews">$[properties.reviews] отзывов</div>' +
    '<div class="price">$[properties.price]</div>' +
    '</div></a>', {
      build: function () {
        myBalloonContentBodyLayout.superclass.build.call(this);
      },
      clear: function () {
        myBalloonContentBodyLayout.superclass.clear.call(this);
      }
    });

  var myBalloonContentLayout = ymaps.templateLayoutFactory.createClass('<div сlass="hotel_balloon" style="position: absolute; left: -120px; bottom:20px;">$[[options.contentBodyLayout]]</div>');

  hotels_array.forEach(function (item, index) {

    searchMarkers[index] = new ymaps.Placemark(item.cords, {
      name: item.name,
      price: item.price,
      reviews: item.reviews,
      image: item.image,
      link: item.link,
      iconContent: '',
      stars: item.stars,
    }, {
      iconLayout: 'default#imageWithContent',
      iconContentLayout: templateDefault,
      iconShape: {
        type: 'Rectangle',
        coordinates: [
          [0, 0],
          [42, 42],
        ]
      },
      balloonContentBodyLayout: myBalloonContentBodyLayout,
      balloonLayout: myBalloonContentLayout,
      balloonPanelMaxMapArea: 0,
      hideIconOnBalloonOpen: false,
    });

    searchMarkers[index].events.add('click', function () {
      var cords = searchMarkers[index].geometry.getCoordinates();

      if(document.documentElement.clientWidth > 992) {
        cords[0] = cords[0] + 0.5;
        cords[1] = cords[1] - 0.5
      } else if(document.documentElement.clientWidth > 576) {
        cords[0] = cords[0] + 1.2;
      } else {
        cords[0] = cords[0] + 0.8;
      }
      search_map.panTo(cords, {
          delay: 0
      });
    });

    search_map.geoObjects.add(searchMarkers[index]);
  });
  search_map.container.fitToViewport();

  search_map.setBounds(search_map.geoObjects.getBounds(), {
    checkZoomRange: true
  });

  search_map.geoObjects.events.add('balloonopen', function (e) {

    var starsCount = $('.search_inner .stars').data('fill');
    for(var i = 0; i < starsCount; i++) {
      $('.search_inner .stars .star-item').eq(i).addClass('active');
    }
  });
}

$(document).ready(function () {
  $(window).on('load resize', function () {
    if (search_map) {
      search_map.container.fitToViewport();
    }
  });
});