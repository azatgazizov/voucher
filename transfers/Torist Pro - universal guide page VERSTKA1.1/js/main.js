(function ($) {
  // пишем функцию с именем нашего плагина
  $.fn.touchanddrag = function () {
    // оборачиваем содержимое нашего элемента в дочерний элемент, который и будем перемещать
    // исходный же элемент сохраняет свою разметку и свойства, заданные ему в css
    this.wrapInner('<div>');
    // исходный элемент теперь стал контейнером, а новый элемент - обертка для его данных
    // то есть исходный элемент (box) - родитель, а новый элемент (data) - дочерний
    var box = this,
      data = this.children();
    // прячем полосу прокрутки
    box.css({
      overflow: 'hidden'
    });
    // позиционируем элемент data
    data.css({
      position: 'absolute',
      cursor: 'default'
    });
    // событие касания на элементе
    data.mousedown(function (e) {
      // высота элементов для дальнейших вычислений
      var hgtBox = box.height(),
        hgtData = data.height();
      // проверяем, достаточно ли контента для прокрутки
      if (hgtData > hgtBox) {
        // позиция касания
        var posTap = e.pageY,
          // позиция элемента data относительно элемента box
          posData = data.position().top,
          posShift,
          // событие скольжения в пределах документа
          mouseMove = function (e) {
            // проверяем, нажата ли еще кнопка мыши
            if (e.which == 1) {
              // расстояние, пройденное относительно первого касания
              posShift = e.pageY - posTap;
              // если прокрутили контент выше верхнего края
              if (data.position().top > 0) {
                // перемещаем контент, но в 5 раз медленнее
                // фрагмент имитации кинетической прокрутки
                data.css({
                  top: (posData + posShift) / 5
                });
                // если прокрутили контент ниже нижнего края
              } else if ((data.position().top + hgtData) < hgtBox) {
                // замедляем перемещение в пять раз
                data.css({
                  top: (hgtBox - hgtData) + (posShift / 5)
                });
                // прокрутка контента в пределах видимости родителя
              } else {
                // добавляем разницу к предыдущим координатам
                data.css({
                  top: posData + posShift
                });
              }
            } else {
              mouseUp();
            }
          },
          // событие отпускания
          mouseUp = function () {
            // отменяем мониторинг перетаскивания и блокировку выделения
            $(document).off('mousemove', mouseMove).off('mouseup', mouseUp);
            $(document).off('mousedown', selection);
            // возвращаем вид курсора
            data.css({
              cursor: 'default'
            });
            // если после прокрутки контент оказался выше верхнего края						
            if (data.position().top > 0) {
              // плавно возвращаем его в крайнюю верхнюю позицию
              // фрагмент имитации кинетической прокрутки
              data.animate({
                top: 0
              }, 250);
              // если после прокрутки контент оказался ниже нижнего края
            } else if ((data.position().top + hgtData) < hgtBox) {
              // плавно возвращаем его в крайнюю нижнюю позицию
              data.animate({
                top: hgtBox - hgtData
              }, 250);
            }
          },
          // снятие выделения при перетаскивании контента
          selection = function () {
            if (window.getSelection) {
              window.getSelection().removeAllRanges()
            } else {
              document.selection.empty()
            }
            return false;
          };
        // меняем вид курсора на время перетаскивания
        data.css({
          cursor: 'move'
        });
        // инициализируем мониторинг перетаскивания и блокировку выделения
        $(document).on('mousedown', selection).on('mousemove', mouseMove);
        $(document).on('mouseup', mouseUp).on('contextmenu', mouseUp);
        $(window).on('blur', mouseUp);
      }
    });
    return this;
  };
})(jQuery);


$(document).ready(function () {
  if ($(".top_slider .top_slider_wrapper").length) {
    $(".top_slider .top_slider_wrapper").owlCarousel({
      loop: true,
      items: 1,
      nav: true,
      navContainer: ".top_slider .nav_container .container",
      dots: false,
      autoplay: true,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      smartSpeed: 1000,
      autoplayTimeout: 6000,
    });
  }
  if ($('.filter_hotels .stars .star-item').length) {
    $('.filter_hotels .stars .star-item').on('click', function () {
      if (!$(this).nextAll('.active').length) {
        $(this).toggleClass('active');
      }
      $(this).prevAll().addClass('active');
      $(this).nextAll().removeClass('active');
    });
  }

  if ($('.search_stars .stars .star-item').length) {
    $('.search_stars .stars .star-item').on('click', function () {
      if (!$(this).nextAll('.active').length) {
        $(this).toggleClass('active');
      }
      $(this).prevAll().addClass('active');
      $(this).nextAll().removeClass('active');
    });
  }

  if ($(".rating_right .rating_right_btn.like").length) {
    $(".rating_right .rating_right_btn.like").on("click", function () {
      $(this).toggleClass("active");
    });
  }

  if ($('.map_search_form')) {
    $('.map_search_form select').styler();
  }

  if ($('.price_filter_body .filter_line').length) {
    $('.price_filter_body .filter_line').slider({
      range: true,
      min: $('.price_filter_body .filter_line').data('min'),
      max: $('.price_filter_body .filter_line').data('max'),
      values: [$('.price_filter_body .filter_line').data('min'), $('.price_filter .filter_line').data('max')],
      stop: function (event, ui) {
        $("input#price_from_value").val($('.price_filter .filter_line').slider("values", 0));
        $("input#price_to_value").val($('.price_filter .filter_line').slider("values", 1));
      },
      slide: function (event, ui) {
        $("input#price_from_value").val($('.price_filter .filter_line').slider("values", 0));
        $("input#price_to_value").val($('.price_filter .filter_line').slider("values", 1));
      }
    });
  }

  $('.checkbox_item input[type="checkbox"]').on('change', function () {
    $(this).parent().toggleClass('active');
  })

  if ($(".search_block .filter_line").length) {
    $(".search_block .filter_line").slider({
      range: true,
      min: $(".search_block .filter_line").data('min'),
      max: $(".search_block .filter_line").data('max'),
      values: [$(".search_block .filter_line").data('min'), $(".search_block .filter_line").data('max')],
      stop: function (event, ui) {
        $(".search_block .price_from").text($('.price_filter .filter_line').slider("values", 0));
        $(".search_block .price_to").text($('.price_filter .filter_line').slider("values", 1));
      },
      slide: function (event, ui) {
        $(".search_block .price_from").text($('.price_filter .filter_line').slider("values", 0));
        $(".search_block .price_to").text($('.price_filter .filter_line').slider("values", 1));
      }
    });
  }

  $('.filter_checkbox_group_items').each(function () {
    if ($(this).children().length > 4) {
      $(this).children().eq(3).nextAll().hide();
      $(this).parent().find('.show_more').addClass('active');
    }
  });

  $('.filter_checkbox_group .show_more.active').on('click', function () {
    $(this).prev().children().eq(3).nextAll().toggle();
    $(this).toggleClass('open');
    $(this).text($(this).hasClass('open') ? 'Свернуть' : 'Развернуть');
  });

  $('#price_to_value').on('change', function () {
    var value_to = parseInt($(this).val());
    var value_from = parseInt($('#price_from_value').val());

    if (value_to > $('.price_filter .filter_line').data('max')) {
      value_to = $('.price_filter .filter_line').data('max');
      $(this).val($('.price_filter .filter_line').data('max'));
    }

    if (value_from > value_to) {
      value_to = value_from;
      $(this).val(value_to);
    }

    $('.price_filter .filter_line').slider('values', [value_from, value_to]);
  });

  $('#price_to_value').on('keyup keydown', function () {
    $(this).val($(this).val().replace(/\D+/g, ''));
  });

  $('#price_from_value').on('keyup keydown', function () {
    $(this).val($(this).val().replace(/\D+/g, ''));
  });

  $('#price_from_value').on('change', function () {
    var value_to = parseInt($('#price_to_value').val());
    var value_from = parseInt($(this).val());

    if (value_from < $('.price_filter .filter_line').data('min')) {
      value_from = $('.price_filter .filter_line').data('min');
      $(this).val(value_from);
    }

    if (value_from > value_to) {
      value_from = value_to;
      $(this).val(value_to);
    }

    $('.price_filter .filter_line').slider('values', [value_from, value_to]);
  });

  var declinationMOnthName = [
    "января",
    "февраля",
    "марта",
    "апреля",
    "мая",
    "июня",
    "июля",
    "августа",
    "сентября",
    "октября",
    "ноября",
    "декабря",
  ];
  var weekNames = [
    "среда",
    "четверг",
    "пятница",
    "суббота",
    "воскресенье",
    "понедельник",
    "вторник",
  ];

  $.datepicker.setDefaults($.datepicker.regional["ru"]);

  $('.filter_block').on('click', function () {
    $(this).find('input[type="text"]').datepicker("show");
  })

  $('.filter_block input[type="text"]').on('focus click', function (e) {
    e.preventDefault();
    $(this).blur();
  });

  if ($("input#date_in").length) {
    var min_date_in = $("#date_in").data("mindate") ?
      new Date($("#date_in").data("mindate")) :
      new Date();
    var max_date_in = $("#date_in").data("maxdate") ?
      new Date($("#date_in").data("maxdate")) :
      "";

    $("#date_in").datepicker({
      showOn: "click",
      minDate: min_date_in,
      maxDate: max_date_in,
      onSelect: function (prop, item) {
        var day = item.selectedDay;
        var month = item.selectedMonth;
        var year = item.selectedYear;
        var week = weekNames[new Date(year + "-" + month + "-" + day).getDay()];
        week = week.substr(0, 1).toUpperCase() + week.substr(1);
        $(this)
          .parent()
          .find(".filter_block_value_bold")
          .text(day + " " + declinationMOnthName[month] + " " + year);
        $(this).parent().find(".filter_block_value").text(week);
        $("#date_out").datepicker(
          "option",
          "minDate",
          parseInt(prop.substr(0, 2)) + 1 + prop.substr(2),
        );
      },
    });
  }
  if ($("input#date_out").length) {
    var min_date_out = $("#date_out").data("mindate") ?
      new Date($("#date_out").data("mindate")) :
      new Date();
    var max_date_out = $("#date_out").data("maxdate") ?
      new Date($("#date_out").data("maxdate")) :
      "";
    $("#date_out").datepicker({
      showOn: "click",
      minDate: min_date_out,
      maxDate: max_date_out,
      onSelect: function (prop, item) {
        var day = item.selectedDay;
        var month = item.selectedMonth;
        var year = item.selectedYear;
        var week = weekNames[new Date(year + "-" + month + "-" + day).getDay()];
        week = week.substr(0, 1).toUpperCase() + week.substr(1);
        $(this)
          .parent()
          .find(".filter_block_value_bold")
          .text(day + " " + declinationMOnthName[month] + " " + year);
        $(this).parent().find(".filter_block_value").text(week);
        $("#date_in").datepicker("option", "maxDate", prop);
      },
    });
  }

  if ($('input#date_hotels_in').length) {
    $('input#date_hotels_in').datepicker({
      showOn: "click",
      minDate: new Date(),
      onSelect: function (prop, item) {
        var day = item.selectedDay;
        var month = item.selectedMonth;
        var year = item.selectedYear;
        var week = weekNames[new Date(year + "-" + month + "-" + day).getDay()];
        week = week.substr(0, 1).toUpperCase() + week.substr(1);
        $(this)
          .parent()
          .find(".filter_block_value_bold")
          .text(day + " " + declinationMOnthName[month] + " " + year);
        $(this).parent().find(".filter_block_value").text(week);
        $("#date_hotels_out").datepicker("option", "minDate", prop);
      },
    });
  }

  if ($('input#date_hotels_out').length) {
    $('input#date_hotels_out').datepicker({
      showOn: "click",
      minDate: new Date(),
      onSelect: function (prop, item) {
        var day = item.selectedDay;
        var month = item.selectedMonth;
        var year = item.selectedYear;
        var week = weekNames[new Date(year + "-" + month + "-" + day).getDay()];
        week = week.substr(0, 1).toUpperCase() + week.substr(1);
        $(this)
          .parent()
          .find(".filter_block_value_bold")
          .text(day + " " + declinationMOnthName[month] + " " + year);
        $(this).parent().find(".filter_block_value").text(week);
        $("#date_hotels_in").datepicker("option", "maxDate", prop);
      },
    });
  }

  if ($("#quests_count").length) {
    $("#quests_count").styler({
      selectVisibleOptions: 10,
      selectPlaceholder: "",
    });
    $("#quests_count").on("change", function () {
      var optValue = $("#quests_count option:selected").val();
      if (optValue % 10 == 1 && optValue != 11) {
        $(".filter_block_props .quests_postfix").text("гость");
      } else if (optValue > 10 && optValue < 20) {
        $(".filter_block_props .quests_postfix").text("гостей");
      } else {
        $(".filter_block_props .quests_postfix").text("гостя");
      }
    });
  }

  if ($('#city_select').length) {
    $('#city_select').styler({
      selectVisibleOptions: 10,
      selectPlaceholder: "",
    });
  }

  if ($("#childs_count").length) {
    $("#childs_count").styler({
      selectVisibleOptions: 20,
      selectPlaceholder: "",
    });
  }

  $(".hotel_rooms_tabs_nav .list").on("click", function () {
    if ($(this).is(".active")) {
      return;
    }
    $('.hotel_view_content').scrollbar('destroy');

    $(".hotel_rooms_tabs_nav .item").removeClass("active");
    $(this).addClass("active");

    // if ($(".hotel_rooms_content .hotel_rooms_table_items_sl_wr").is(".owl-loaded")) {
    // $(".hotel_rooms_content .hotel_rooms_table_items_sl_wr").trigger("destroy.owl.carousel");
    // $('.hotel_view_content').getNiceScroll().remove();
    // $(".hotel_rooms_content .hotel_rooms_table_items_sl_wr").removeClass("owl-carousel"); 
    $(".hotel_rooms_content").removeClass("hotel_rooms_table_items");
    $(".hotel_rooms_content").addClass("hotel_rooms_list_items");
    // }
  });

  $(".hotel_rooms_tabs_nav .table").on("click", function () {
    if ($(this).is(".active")) {
      return;
    }

    $(".hotel_rooms_tabs_nav .item").removeClass("active");
    $(this).addClass("active");

    // if (!$(".hotel_rooms_content .hotel_rooms_table_items_sl_wr").is(".owl-loaded")) {
    $(".hotel_rooms_content").removeClass("hotel_rooms_list_items");
    $(".hotel_rooms_content").addClass(
      "hotel_rooms_table_items",
    );
    // $('.hotel_rooms_table_items .hotel_view_content').css({
    //   "width": (document.documentElement.clientWidth - $('.hotel_rooms_content').offset().left - 10)
    // })
    $('.hotel_view_content').scrollbar({
      disableBodyScroll: true,
      stepScrolling: true,
      scrollStep: 40,
      scrollx: 'advanced',
      duration: '250',
    });
    if (document.documentElement.clientWidth > 576) {
      $('.hotel_rooms_table_items .hotel_view_content').css({
        "width": (document.documentElement.clientWidth - $('.hotel_rooms_content').offset().left)
      });
    } else {
      $('.hotel_rooms_table_items .hotel_view_content').css({
        'width': '100%'
      });
    }
    // $(".hotel_view_content").niceScroll({
    //   autohidemode: false,
    //   cursorborder: false,
    //   cursorwidth: '4px',
    //   cursorborderradius: '0',
    //   cursorcolor: '#c8c8c8',
    //   background: '#f4f4f4',
    //   railvalign: 'top',
    //   grabcursorenabled: true,
    //   emulatetouch: true,
    //   preventmultitouchscrolling: false,
    //   cursordragontouch: true,
    //   smoothscroll: false,
    //   enablescrollonselection: false,
    // });


    // $('.hotel_rooms_table_items_sl_wr').addClass('owl-carousel');
    // $(".hotel_rooms_table_items_sl_wr").owlCarousel({
    //   dots: false,
    //   nav: false,
    //   items: 1,
    //   responsiveClass: true,
    //   loop: true,
    //   responsive: {
    //     1430: {
    //       items: 4
    //     },
    //     576: {
    //       items: 3
    //     }
    //   }
    // });
    // $(".hotel_rooms_content .owl-item.active").eq(3).addClass("opacity");
    // }
    // var window_wz = document.documentElement.clientWidth;
    // var index = 3;
    // if (window_wz < 1430) {
    //   index = 2;
    // }
    // if (window_wz < 768) {
    //   index = 5;
    // }
    // $(".hotel_rooms_content .owl-item.active").eq(index).addClass("opacity");

    // $(".hotel_rooms_content .hotel_rooms_table_items_sl_wr").on(
    //   "translated.owl.carousel initialized.owl.carousel",
    //   function () {
    //     window_wz = document.documentElement.clientWidth;
    //     $(".hotel_rooms_content .owl-item").removeClass("opacity");
    //     index = 3;
    //     if (window_wz < 1430) {
    //       index = 2;
    //     }
    //     if (window_wz < 768) {
    //       index = 5;
    //     }
    //     $(".hotel_rooms_content .owl-item.active").eq(index).addClass("opacity");
    //   },
    // );
  });

  $(".hotel_rooms_nav .nav_item").on("click", function () {
    if ($(this).is("active")) {
      return;
    }

    $('.select_bank_card select').trigger('refresh');

    $(".hotel_rooms_nav .nav_item").removeClass("active");
    $(".hotel_rooms_tab_item").removeClass("active");
    $(".hotel_rooms_tab_item").eq($(this).index()).addClass("active");

    $(this).addClass("active");
  });
  if ($('.hotel_view_content').length) {
    $('.hotel_view_content').mousewheel(function (e, delta) {
      if ($('.hotel_rooms_table_items').length) {
        this.scrollLeft -= (delta * 60);
        e.preventDefault();
      }
    });
  }
  $(window).on('resize load', function () {
    if ($('.hotel_rooms_table_items .hotel_view_content').length) {
      if (document.documentElement.clientWidth > 576) {
        $('.hotel_rooms_table_items .hotel_view_content').css({
          "width": (document.documentElement.clientWidth - $('.hotel_rooms_content').offset().left)
        });
      } else {
        $('.hotel_rooms_table_items .hotel_view_content').css({
          'width': '100%'
        });
      }
    }
  });

  $(".popup_sl_images").owlCarousel({
    dots: false,
    nav: true,
    items: 1,
  });

  $('.form_radio_group input[type="radio"]').on("change", function () {
    $(this).parent().parent().find("label").removeClass("active");
    $(this).parent().toggleClass("active");
  });

  $('.select_bank_card select').styler({
    selectVisibleOptions: 13,
  });

  $('.reservation_transfer_label input[type="radio"]').on(
    "change",
    function () {
      $(this)
        .parent()
        .parent()
        .parent()
        .find(".reservation_transfer_item_wr")
        .removeClass("active");
      $(this).parent().parent().addClass("active");
    },
  );
  $('.card_code input').on('focus', function () {
    $(this).parent().addClass('focus');
  })

  $('.card_code input').on('blur', function () {
    $(this).parent().removeClass('focus');
  })

  $('.reservation_payments_nav .item').on('click', function () {
    if ($(this).is('.active')) {
      return;
    }
    $('.reservation_payments_nav .item').removeClass('active');
    $(this).addClass('active');
    $('.reservation_payments_tab').removeClass('active');
    $('.reservation_payments_tab').eq($(this).index()).addClass('active');
  });

  $('.finaly_block_agreement input[type="checkbox"]').on('change', function () {
    $(this).parent().toggleClass('active');
  });

  $('.card_code input').mask('999');
  $("input.telephone").mask("+7(999) 999 99 99");
  $('.bank_card input').mask('9999   9999    9999   9999');

  $('.mobile_menu_icon').on('click', function () {
    $(this).toggleClass('active');
    $('.mobile_menu').toggleClass('active');
    if ($(this).is('.active')) {
      $('body').css({
        'overflow': 'hidden'
      });
    } else {
      $('body').css({
        'overflow': 'auto'
      });
    }
  });

  if ($('.hotel_items_line').length) {
    // $('.hotel_items_line').niceScroll({
    //   autohidemode: false,
    //   cursorborder: false,
    //   cursorwidth: '3px',
    //   cursorborderradius: '0',
    //   cursorcolor: '#c8c8c8',
    //   background: '#f4f4f4',
    //   scrollbarid: 'nicescroll-rails-hr',
    //   emulatetouch: true,
    //   preventmultitouchscrolling: false,
    //   cursordragontouch: true,
    //   smoothscroll: false,
    //   enablescrollonselection: false,
    // });
  }
  // $(document).on('scroll', function () {
  //   var wind_wz = document.documentElement.clientWidth;
  // })

  $('.hotels_list_sidebar .hotels_sidebar_nav .list').on('click', function () {
    if (!$(this).is('.active')) {
      $('.hotels_list_sidebar .hotels_sidebar_nav .item').removeClass('active');
      $(this).addClass('active');
      $('.hotel_map_items').addClass('hotels_list').removeClass('hotel_map_items');
      // $('.hotel_items_line').getNiceScroll().remove();
      $('.hotel_items_line').addClass('hotel_list_items').removeClass('hotel_items_line');
      $('.hotel_items_line_wrapper').addClass('hotel_list_wrapper').removeClass('hotel_items_line_wrapper');
    }
  });



  $('.hotels_list_sidebar .hotels_sidebar_nav .map').on('click', function () {
    if (!$(this).is('.active')) {
      $('.hotels_list_sidebar .hotels_sidebar_nav .item').removeClass('active');
      $(this).addClass('active');
      $('.hotels_list').addClass('hotel_map_items').removeClass('hotels_list');
      $('.hotel_list_items').addClass('hotel_items_line').removeClass('hotel_list_items');

      // $('.hotel_items_line').niceScroll({
      //   autohidemode: false,
      //   cursorborder: false,
      //   cursorwidth: '3px',
      //   cursorborderradius: '0',
      //   cursorcolor: '#c8c8c8',
      //   background: '#f4f4f4',
      //   emulatetouch: true,
      //   preventmultitouchscrolling: false,
      //   cursordragontouch: true,
      //   smoothscroll: false,
      //   enablescrollonselection: false,
      // });
      $('.hotel_list_wrapper').addClass('hotel_items_line_wrapper').removeClass('hotel_list_wrapper');
      var wind_wz = document.documentElement.clientWidth;
      if (wind_wz > 1200 && $('.hotel_items_line_wrapper').length) {
        $('#hotels_map').css({
          'width': $('.hotel_items_line_wrapper').offset().left
        });
      } else {
        $('#hotels_map').css({
          'width': '100%'
        });
      }
      main_map.container.fitToViewport();
      main_map.setBounds(main_map.geoObjects.getBounds(), {
        checkZoomRange: true
      });
    }

    


  });

  // $('.child_ages .ages_select select').styler({
  //   selectVisibleOptions: 5
  // });

  $('.filter_item .filter_block_value_bold.quests').on('click', function (e) {
    e.stopPropagation();
    if ($(this).parent().parent().parent().find('.float_block')) {
      if ($('.clients_select.childs input').val() > 0) {
        $('.float_block_bottom').addClass('has_child');
      } else {
        $('.float_block_bottom').removeClass('has_child');
      }
      $(this).parent().parent().parent().toggleClass('active');
      $(this).parent().parent().parent().find('.float_block').slideToggle();
    }
  })

  $('.add_block .minus').on('click', function () {
    var value = $(this).parent().find('input').val();
    value -= 1;
    value = value >= 0 ? value : 0;
    $(this).parent().find('input').val(value);
  });

  $('.add_block .plus').on('click', function () {
    $(this).parent().find('input').val(parseInt($(this).parent().find('input').val()) + 1);
  });

  $('.clients_select input').on('keydown keyup', function () {
    $(this).val($(this).val().replace(/\D+/g, ''));
  });

  var options = '';

  for (var i = 1; i < 18; i++) {
    if (i == 1) {
      age_desc = ' год';
    } else if (i < 5) {
      age_desc = ' года';
    } else {
      age_desc = ' лет'
    }
    options += '<option value="' + i + '">' + i + age_desc + '</option>'
  }

  var childs_age = $('<div class="child_ages">' +
    '<div class="childs_count"></div>' +
    '<div class="ages_select">' +
    '<select>' +
    options +
    '</select>' +
    '</div>' +
    '</div>');

  $('.childs input').on('change', function () {
    if ($('.child_ages').length > $(this).parent().find('input').val()) {
      for (var i = $('.child_ages').length; i > $(this).parent().find('input').val(); i--) {
        $('.child_ages').eq(i).remove();
      }
    } else if ($('.child_ages').length < $(this).parent().find('input').val()) {
      for (var i = $('.child_ages').length; i < $(this).parent().find('input').val(); i++) {
        var clone = childs_age.clone();
        clone.find('.childs_count').text((i + 1) + ' ребенок');
        $('.float_block_bottom').append(clone);
        clone.find('select').styler({
          selectVisibleOptions: 5
        });
      }
    }

    if ($(this).val() == 0) {
      $('.float_block_bottom').removeClass('has_child');
    } else if ($(this).val() > 0) {
      $('.float_block_bottom').addClass('has_child');
    }
  });

  $('.childs .control').on('click', function () {
    if ($(this).parent().find('input').val() > 0) {
      if ($('.child_ages').length > $(this).parent().find('input').val()) {
        if (!$(this).parent().find('input').val()) {
          $('.child_ages').remove();
        } else {
          $('.child_ages').eq($(this).parent().find('input').val()).remove();
        }
      } else if ($('.child_ages').length < $(this).parent().find('input').val()) {
        var clone = childs_age.clone();
        clone.find('.childs_count').text($(this).parent().find('input').val() + ' ребенок');
        $('.float_block_bottom').append(clone);
        clone.find('select').styler({
          selectVisibleOptions: 5
        });
      }
    }
    if ($(this).parent().find('input').val() > 0) {
      $('.float_block_bottom').addClass('has_child');
    } else if ($(this).parent().find('input').val() == 0) {
      $('.float_block_bottom').removeClass('has_child');
    }
  });
  $('.float_block_submit').on('click', function (e) {
    e.preventDefault();
    var adult = $(this).parent().find('input[name="adults"]').val();
    var childs = $(this).parent().find('input[name="childs"]').val();
    $(this).parent().parent().find('.filter_block_value_bold').text(adult + ' взрослых, ' + childs + ' детей');
    $(this).parent().parent().removeClass('active');
    $(this).parent().slideUp();
  });

  $('.clients_select input').on('change', function () {
    if (!$(this).val()) {
      $(this).val('0');
    }
  });

  $('.header .btn_search input').on('focus', function () {
    $(this).parent().addClass('active');
  });

  $('.header .btn_search input').on('blur', function () {
    $(this).parent().removeClass('active');
  })

  $('.city_name select').styler();

  $('.my_reservation select').styler();

  $(window).on('load resize', function () {
    var window_wz = document.documentElement.clientWidth;
    if ($('.hotel_view_content').lenght) {
      if (window_wz > 576) {
        $('.hotel_rooms_table_items .hotel_view_content').css({
          "width": (window_wz - $('.hotel_rooms_content').offset().left - 10)
        });
      } else {
        $('.hotel_rooms_table_items .hotel_view_content').css({
          'width': '100%'
        });
      }
    }
  });
});

