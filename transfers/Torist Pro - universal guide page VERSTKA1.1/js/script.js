$(document).ready(function(){
  //Слайдер предложения
  $(".offers_slider .owl-carousel").owlCarousel({
    //items: 1,
    margin: 7,
    loop: true,
    responsive : {
    0: {
          items: 1,
          stagePadding: 20,
          center:true
    },
    556: {
        items: 2,
        stagePadding: 0,
    },
    762: {
        items: 2,
        stagePadding: 0,

    },
    992: {
        items: 3,
        stagePadding: 0,
    },
    1200 : {
        items: 4,
        stagePadding: 60,
    },
    1500 : {
        items: 5,
        stagePadding: 60,
    },
    1600: {
      items: 6,
      stagePadding: 60,
    }
  }
  });
  $(".popular_arrow_left").on("click", function(){
    $(".offers_slider .owl-carousel").trigger("prev.owl.carousel")
  });

  $(".popular_arrow_right").on("click", function(){
    $(".offers_slider .owl-carousel").trigger("next.owl.carousel")
  });

  //Лучшие предложения
  $(".best_offers_slider .owl-carousel").owlCarousel({
    items: 4,
    margin: 15,
    stagePadding: 23,
    loop: true,
    nav: true,
    responsive : {
    0: {
          items: 1,
          stagePadding: 0,
    },
    320: {
        items: 1,
        stagePadding: 0,
    },
    450: {
        items: 2,
        stagePadding: 0,
    },
    992: {
        items: 3,
        stagePadding: 0,
    },
    1500 : {
        items: 4,
        stagePadding: 23,
    }
  }

  });
  $(".best_arrow_left").on("click", function(){
    $(".best_offers_slider .owl-carousel").trigger("prev.owl.carousel")

  });

  $(".best_arrow_right").on("click", function(){
    $(".best_offers_slider .owl-carousel").trigger("next.owl.carousel")
  });


  //Слайдер путеводитель
  $(".guide_slider_wrapper .owl-carousel").owlCarousel({
    items: 4,
    margin: 30,
    stagePadding: 47,
    loop: true,
    responsive : {
    0: {
        margin: 0,
        items: 1,
        stagePadding: 0,
    },
    320: {
        margin: 0,
        items: 1,
        stagePadding: 0,
    },
    450: {
        items: 2,
        stagePadding: 0,
    },
    992: {
        items: 3,
        stagePadding: 0,
    },
    1500 : {
        items: 4,
        stagePadding: 23,
    }
  }
  });
  $(".guide_arrow_left").on("click", function(){
    $(".guide_slider_wrapper .owl-carousel").trigger("prev.owl.carousel")
  });

  $(".guide_arrow_right").on("click", function(){
    $(".guide_slider_wrapper .owl-carousel").trigger("next.owl.carousel")
  });

  //Слайдер guide_in top
  $(".guide_in_top_slider .owl-carousel").owlCarousel({
    items: 1,
    loop: true,
  });
  $(".guide_in_top_arrow_left").on("click", function(){
    $(".guide_in_top_slider .owl-carousel").trigger("prev.owl.carousel")
  });

  $(".guide_in_top_arrow_right").on("click", function(){
    $(".guide_in_top_slider .owl-carousel").trigger("next.owl.carousel")
  });

  //Слайдер guide_in bottom
  $(".guide_in_bottom_slider .owl-carousel").owlCarousel({
    items: 1,
    loop: true,
    nav: false,
  });

  $(".guide_in_bottom_arrow_left").on("click", function(){
    $(".guide_in_bottom_slider .owl-carousel").trigger("prev.owl.carousel")
  });

  $(".guide_in_bottom_arrow_right").on("click", function(){
    $(".guide_in_bottom_slider .owl-carousel").trigger("next.owl.carousel")
  });





$( ".lk_user_menu_show" ).click(function(){
   $( ".lk_user_menu ul" ).slideToggle();
 });

 $(function() {

   $('.styler').styler();

 });


$('input:radio').change(function () {
      if ($(this).parent().hasClass('checked')) {
        $('label').removeClass('checked')
        $(this).closest('label').addClass('checked');
      }else{
        $(this).closest('label').removeClass('checked');
      }
})

$(function(){

  $("#lk_user_main_form_phone, .contacts_f_n").mask("+7 999 999-99-99");
  $("#lk_user_pasport_form_pas_number").mask("99 99 999 999");
  $("#lk_user_pasport_form_code").mask("9999 9999");
  $("#lk_user_pasport_form_lcode").mask("9999 9999");

  $("#lk_user_card_form_number").mask("9999 9999 9999 9999");
  $("#lk_user_card_form_defcode").mask("999");

});

$('.payment_form_wrapper .nav_item').on('click', function() {
  if($(this).is('active')) {
    return;
  }

  $('.payment_form_wrapper .nav_item').removeClass('active');
  $('.payment_tab_item').removeClass('active');
  $('.payment_tab_item').eq($(this).index()).addClass('active');

  $(this).addClass('active');
});

$('.guide_menu_show').click(function () {
  $('.guide_menu ul').slideToggle();
})

$('[data-fancybox="images"]').fancybox({
  margin: 0,
  infobar : false,
  buttons : false,
  clickContent : false,
  arrows : false,
  animationEffect : "fade",
  idleTime : false,
  thumbs : {
    autoStart: true
  },

  afterLoad : function(instance, current) {
    $('.fancybox-image-wrap').append('<a data-fancybox-prev class="fb_left"></a>');
    $('.fancybox-image-wrap').append('<a data-fancybox-next class="fb_right"></a>');
    $('.fancybox-image-wrap').append('<a data-fancybox-close class="fb_close"></a>');
  },
});



});
