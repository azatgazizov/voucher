var mapMarkers = {};
var main_map;

ymaps.ready(init);

function init() {

  main_map = new ymaps.Map("hotels_map", {
    center: [55.76, 37.64],
    zoom: 10,
    controls: ['zoomControl'],
  });

  main_map.events.add('click', function () {
    main_map.balloon.close();
  });

  main_map.behaviors.disable('scrollZoom');

  var templateDefault = ymaps.templateLayoutFactory.createClass(
    '<div class="{% if properties.liked %} mark_restaurant {% else %} mark_hotel {% endif %}">{% if properties.liked %}<img style="width: 21.5px;" src="images/mark_meal.svg">{% endif %}$[properties.iconContent]</div>'
  );

  var templateOrange = ymaps.templateLayoutFactory.createClass(
    '<div class="mark_restaurant">$[properties.iconContent]</div>'
  );

  var myBalloonContentBodyLayout = ymaps.templateLayoutFactory.createClass(
    '<a class="baloon_inner" href="$[properties.link]"><div class="baloon_sub_title"><div class="balloon_name">$[properties.name]</div><div class="baloon_review">$[properties.reviews] отзывов</div></div><div class="baloon_image"><img src="$[properties.image]"/></div><div class="baloon_empty">$[properties.free] метров от вас </div></a>', {
      build: function () {
        myBalloonContentBodyLayout.superclass.build.call(this);
      },
      clear: function () {
        myBalloonContentBodyLayout.superclass.clear.call(this);
      }
    });

  var myBalloonContentLayout = ymaps.templateLayoutFactory.createClass('<div сlass="hotel_balloon" style="position: absolute; left: -70px; bottom:20px;">$[[options.contentBodyLayout]]</div>');

  hotels_array.forEach(function (item) {
    var shapeSize = [100, 30];
    if (item.liked) {
      shapeSize = [124.5, 35];
    }
    mapMarkers[item['id']] = new ymaps.Placemark(item.cords, {
      name: item['name'],
      price: item['price'],
      image: item['image'],
      reviews: item['reviews'],
      free: item['free'],
      iconContent: item['price'],
      link: item['link'],
      liked: item['liked'],
    }, {
      iconLayout: 'default#imageWithContent',
      iconContentLayout: templateDefault,
      iconShape: {
        type: 'Rectangle',
        coordinates: [
          [0, 0],
          shapeSize,
        ]
      },
      balloonContentBodyLayout: myBalloonContentBodyLayout,
      balloonLayout: myBalloonContentLayout,
      balloonPanelMaxMapArea: 0,
      hideIconOnBalloonOpen: false,
    });

    mapMarkers[item['id']].events.add('click', function () {
      var cords = mapMarkers[item['id']].geometry.getCoordinates();
      if(document.documentElement.clientWidth < 768) {
        cords[0] = cords[0] + 1;
        cords[1] = cords[1] + 0.5;
      }
      main_map.panTo(cords, {
          delay: 0
      });
    });

    main_map.geoObjects.add(mapMarkers[item['id']]);
  });
  main_map.container.fitToViewport();

  main_map.setBounds(main_map.geoObjects.getBounds(), {
    checkZoomRange: true
  });
}


$(document).ready(function () {

  $('.hotel_item_wr > a').on('click', function (e) {
    if($('.hotel_items_line').length) {
      e.preventDefault();
      main_map.balloon.close();
      // main_map.setCenter(mapMarkers[$(this).data('id')].geometry.getCoordinates())
      if (!$(this).find('.hotel_item_inner').is('.active')) {
        $('.hotel_item_inner').removeClass('active');
        if (mapMarkers[$(this).data('id')] != undefined) {
          mapMarkers[$(this).data('id')].events.fire('click');
        }
      }
      $(this).find('.hotel_item_inner').toggleClass('active');
    }
  });

  $(window).on('load resize', function () {
    var wind_wz = document.documentElement.clientWidth;
    if (wind_wz > 1200 && $('.hotel_items_line_wrapper').length) {
      $('#hotels_map').css({
        'width': $('.hotel_items_line_wrapper').offset().left
      });
    } else {
      $('#hotels_map').css({
        'width': '100%'
      });
    }
    main_map.container.fitToViewport();
    main_map.setBounds(main_map.geoObjects.getBounds(), {
      checkZoomRange: true
    });

  });
});


