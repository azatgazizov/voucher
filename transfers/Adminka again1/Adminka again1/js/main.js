$(document).ready(function () {
  if ($('.lang_select select').length) {
    $('.lang_select select').styler({
      selectVisibleOptions: 5,
      selectSmartPositioning: false,
    });
  }

  if ($('.hotels_select_controls .item select').length) {
    $('.hotels_select_controls .item select').styler({
      selectVisibleOptions: 10,
      selectPlaceholder: 'Выбрать',
      selectSmartPositioning: false,
    });
  }

  if ($('.status select').length) {
    $('.status select').styler({
      selectVisibleOptions: 10,
      selectSmartPositioning: false,
    });
  }
  if ($('.tab_controls .item').length) {
    $('.tab_controls .item').on('click', function () {
      if (!$(this).is('.active')) {
        $('.tab_controls .item').removeClass('active');
        $(this).addClass('active');
      }
    });
  }

  if ($('.main_select select').length) {
    $('.main_select select').styler({
      selectVisibleOptions: 10,
      selectPlaceholder: 'Выбрать',
      selectSmartPositioning: false,
    });
  }

  if ($('.services_select select').length) {
    $('.services_select select').styler({
      selectVisibleOptions: 10,
      selectPlaceholder: 'По прайсу',
      selectSmartPositioning: false,
    });
  }



 

  // if ($('.main_tabs_controls .item').length) {
  //   $('.main_tabs_controls .item').on('click', function () {
  //     if (!$(this).is('active')) {
  //       $(this).parent().find('.item').removeClass('active');
  //       $(this).addClass('active');
  //       $('.tabs_wrapper .tab_item').removeClass('active');
  //       $('.tabs_wrapper .tab_item').eq($(this).index()).addClass('active');
  //     }
  //   });
  // }

  if ($('.num_input').length) {
    $('.num_input').numeric({ decimal: ",",  negative : false, scale: 2});
  }

  if($('.input_wrapper.price input[type="text"]')) {
    $('.input_wrapper.price input[type="text"]').numeric({ decimal: ",",  negative : false, scale: 2});
  }

  if($('.phone_input').length) {
    $('.phone_input').mask('+7 999 999 99 99');
  }

  if ($('.input_wrapper').length) {
    $('.input_wrapper .num_input').each(function (index, item) {
      if (!$(this).val() || $(this).val() == '0' || $(this).val() == '0,0' || $(this).val() == '0,00') {
        $(this).val('');
        $(this).parent().parent().removeClass('filled');
      } else {
        $(this).parent().parent().addClass('filled');
      }
    });

    $('.input_wrapper .text_input').each(function (index, item) {
      if ($(item).val()) {
        $(item).parent().parent().addClass('filled');
      } else {
        $(item).parent().parent().removeClass('filled');
      }
    });

    $('.input_wrapper select').each(function(index, item) {
      if($(this).val()) {
        $(this).parent().parent().parent().addClass('filled');
      } else {
        $(this).parent().parent().parent().removeClass('filled');
      }
    });

    $('.input_wrapper .text_input').on('change', function() {
      if ($(this).val()) {
        $(this).parent().parent().addClass('filled');
      } else {
        $(this).parent().parent().removeClass('filled');
      }
    });

    $('.input_wrapper .num_input').on('change', function () {
      if (!$(this).val() || $(this).val() == '0' || $(this).val() == '0,0' || $(this).val() == '0,00') {
        $(this).val('');
        $(this).parent().parent().removeClass('filled');
      } else {
        $(this).parent().parent().addClass('filled');
      }
    });
  }

  if($('.checkbox_main').length) {
    $('.checkbox_main input[type="checkbox"]').on('change', function () {
      $(this).parent().toggleClass('active');
    });
  }

  if($('.checkbox_switch').length) {
    $('.checkbox_switch  input[type="checkbox"]').on('click', function () {
      $(this).parent().toggleClass('active');
    })
  }

  $().fancybox({
    selector: 'a.fb-group',
  });
});

$(document).ready(function () {
  $('[data-toggle="offcanvas"], #navToggle').on('click', function () {
      $('.offcanvas-collapse').toggleClass('open')
  })
});

$('.toggle-menu').click (function(){
  $(this).toggleClass('active');
  $('#menu').toggleClass('open');

});


$('#navToggle').on('click', function (e) {
  e.preventDefault();
  $('.offcanvas-collapse').toggleClass('active');
  return false;
});


$(function(){
  $('.switch-btn').click(function (e, changeState) {
      if (changeState === undefined) {
          $(this).toggleClass('switch-on');
      }
      if ($(this).hasClass('switch-on')) {
          $(this).trigger('on.switch');
      } else {
          $(this).trigger('off.switch');
      }
  });

  $('.switch-btn').on('on.switch', function(){
    $('#menu-1').removeClass('dark');
    $('#d').removeClass('dark-left');

  });
  
  $('.switch-btn').on('off.switch', function(){
    $('#menu-1').addClass('dark');
    $('#d').addClass('dark-left');
    
  });


  $('.switch-btn').each(function(){
      $(this).triggerHandler('click', false);
  });
  
});

var $homeSlider = $(".slide-tabs");

$(window).resize(function() {
  showHomeSlider();
});

function showHomeSlider() {
  if ($homeSlider.data("owlCarousel") !== "undefined") {
    if (window.matchMedia('(max-width: 991px)').matches) {
      initialHomeSlider();
    } else {
      destroyHomeSlider();
    }
  }
}
showHomeSlider();

function initialHomeSlider() {
  $homeSlider.addClass("owl-carousel").owlCarousel({
    loop:true,
    stagePadding: 0,
    margin: 0,
    nav:false,
    responsive:{
        0:{
            items: 3,
            stagePadding: 0,
            margin: 100
        },
        600:{
            items: 4
        },
        800:{
            items: 4,
            margin: 100
           
        },
        900:{
            items: 5,
            margin: 100
        }
    
    }


  });


  
};


function destroyHomeSlider() {
  $homeSlider.trigger("destroy.owl.carousel").removeClass("owl-carousel");
 
};

var $homeSliders = $(".slide-tabs-two");

$(window).resize(function() {
  showHomeSliders();
});

function showHomeSliders() {
  if ($homeSliders.data("owlCarousel") !== "undefined") {
    if (window.matchMedia('(max-width: 991px)').matches) {
      initialHomeSliders();
    } else {
      destroyHomeSliders();
    }
  }
}
showHomeSliders();

function initialHomeSliders() {
  $homeSliders.addClass("owl-carousel").owlCarousel({
    loop:false,
    stagePadding: 0,
    margin: 0,
    nav:false,
    responsive:{
        0:{
            items: 1,
            stagePadding: 0,
            margin: 10
        },
        600:{
            items: 2,
            margin: 10
        },
        800:{
            items: 3,
            margin: 20
           
        },
        900:{
           margin: 10,
            items: 4,
           
        }
    
    }


  });


  
};


function destroyHomeSliders() {
  $homeSliders.trigger("destroy.owl.carousel").removeClass("owl-carousel");
 
};




new Glider(document.querySelector('.glider'), {
  // Mobile-first defaults
  slidesToShow: 2,
  slidesToScroll: 1,
  draggable: true,

  responsive: [
    {
      // screens greater than >= 775px
      breakpoint: 775,
      settings: {
        // Set to `auto` and provide item width to adjust to viewport
        slidesToShow: 'auto',
        slidesToScroll: 'auto',
        itemWidth: 150,
      
      }
    },{
      // screens greater than >= 1024px
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        itemWidth: 200,
      
      }
    }
  ]
});

new Glider(document.querySelector('.glider-two'), {
  // Mobile-first defaults
  slidesToShow: 2,
  slidesToScroll: 1,
  itemWidth: 150,
  draggable: true,


  responsive: [
  {
      // screens greater than >= 775px
      breakpoint: 400,
      settings: {
      
        slidesToShow: 1,
        slidesToScroll: 1,
        duration: 0.25
      }
    },
    
    {
      // screens greater than >= 775px
      breakpoint: 775,
      settings: {
      
        slidesToShow: 'auto',
        slidesToScroll: 'auto',
        duration: 0.25
      }
    },{
      // screens greater than >= 1024px
      breakpoint: 1024,
      settings: {
        slidesToShow: 'auto',
        slidesToScroll: 1,
        itemWidth: 200,
        duration: 0.25
      }
    }
  ]
});





